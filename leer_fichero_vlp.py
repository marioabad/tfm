import os
import json
from datetime import datetime
import func_estadistica
pond1 = 0
pond2 = 0
pond3 = 0
cont = 1
d_resultados = dict()
rentabilidades = []

""" Almacena las mayores rentabilidades según el perfil de riesgo"""
resultados = [] 
ran_vol_conservador = [0.0,0.04]
ran_vol_moderado = [0.04,0.07]
ran_vol_dinamico = 0.07

#print('getcwd:      ', os.getcwd()) 
#print('__file__:    ', __file__) 
#print('basename:    ', os.path.basename(__file__))
#print('dirname:     ', os.path.dirname(__file__)) #Utilizamos esto para no limitar la ubicación del fichero 

"""Abrimos el fichero json que contiene los precios diarios de los fondos 
utilizando una ruta relativa para no limitar la ubicación del fichero"""

def obtener_datos_fich():
    with open (os.path.dirname(__file__) +'/vlps_fondos.txt') as file:
        fichero = json.load (file) 
    file.close
    return fichero

    # TODO controlar excepciones
 
"""Leemos los valores liquidativos del fichero json y calculamos los rendimientos diarios 
con los precios de cada fondo"""

def calcular_rend_diarios():
    cond_fondos = 0
    lista1 = []
    lista2 = []
    lista3 = []
    fichero = obtener_datos_fich()
    for i in fichero ['fondos']:
        #print('Nombre:', i['name'])
        #print('ISIN:', i['isin'])
        cont_rend = 0
        lista = []
        for j in i['vlps']:
            if cont_rend == 0:
                precio_ant = i['vlps'][j]
                cont_rend += 1
            else: 
                rendimiento = ((i['vlps'][j]-precio_ant)/precio_ant)
                precio_ant = i['vlps'][j]
                lista.append(rendimiento)
                
        if cond_fondos == 0: 
            lista1 = lista
            cond_fondos += 1
        elif cond_fondos == 1:
            lista2 = lista
            cond_fondos += 1
        else:
            lista3 = lista

    #print(f'Lista1 es:  {lista1}' )
    #print(f'Lista2 es:  {lista2}' )
    #print(f'Lista3 es: {lista3}' )

    return [lista1, lista2, lista3]

"""Se crea una función que calcule la rentabilidad de un activo entre dos fechas"""

def calcular_rentabilidad (precio_inicial, precio_final):
    rentabilidad =  (precio_final-precio_inicial)/precio_inicial
    return rentabilidad

"""Buscamos las fechas inicio y fin y los precios a estas fechas 
en el fichero json para pasarlas como argumentos a la función calcular_rentabilidad"""

def obtener_rent_fondos():
    lista_rentabilidades = []
    fichero = obtener_datos_fich()
    for i in fichero ['fondos']:
        fecha_inicial = ""
        fecha_final = ""
        for j in i['vlps']:
            fecha_leida = datetime.strptime(j, '%d/%m/%Y')
            if fecha_inicial == "":
                fecha_inicial = fecha_leida
            else: 
                if (fecha_inicial) > fecha_leida:
                    fecha_inicial = fecha_leida
            if str(fecha_final) == "":
                fecha_final = fecha_leida
            else: 
                if (fecha_final) < fecha_leida:
                    fecha_final = fecha_leida
        fecha_i = datetime.strftime(fecha_inicial, '%d/%m/%Y' )
        fecha_f = datetime.strftime(fecha_final, '%d/%m/%Y')
        precio_inicial = i['vlps'][fecha_i]
        precio_final = i['vlps'][fecha_f]
        lista_rentabilidades.append(calcular_rentabilidad(precio_inicial, precio_final))
    #print (f'##############{lista_rentabilidades}')
    return lista_rentabilidades
        #print(i['name'])
        #print(f' fecha inicial {fecha_inicial}, fecha final {fecha_final}, rentabilidad {calcular_rentabilidad(precio_inicial, precio_final)}')
    
#print(f' Varianza fondo 1: {func_estadistica.calcular_varianza(lista1)}')
#print(f' Varianza fondo 2: {func_estadistica.calcular_varianza(lista2)}')
#print(f' Varianza fondo 3: {func_estadistica.calcular_varianza(lista3)}')

#print(f' covarianza fondo 1,2: {func_estadistica.calcular_covarianza(lista1, lista2)}')
#print(f' covarianza fondo 1,3: {func_estadistica.calcular_covarianza(lista1, lista3)}')
#print(f' covarianza fondo 2,3: {func_estadistica.calcular_covarianza(lista2, lista3)}')

""" Buscamos todas las combinaciones de ponderación de fondos y escojemos la de máxima rentabilidad dado 
un rango de volatilidad asignado a cada perfil de riesgo"""

def calcular_carteras_optimas(resultado_test):
    rentabilidades = obtener_rent_fondos()
    max_rent_conservador = None
    max_rent_moderado = None
    max_rent_dinamico = None
    vol_conservador = None
    vol_moderado = None
    vol_dinamico = None
    pond_conservador = []
    pond_moderado = []
    pond_dinamico = []

    for z in range (0,4):
        for i in range (8, 0, -1):
            valor_ini = 10-i-1
            for j in range (valor_ini,0, -1):

                if z == 0:
                    pond = [round(i/10,1),round(1-i/10-j/10,1),round(j/10,1)]
                elif z == 1:
                    pond = [round(1-i/10-j/10,1),round(i/10,1),round(j/10,1)]
                elif z == 2:
                    pond = [round(j/10,1),round(1-i/10-j/10,1),round(i/10,1)]
                elif z == 3:
                    pond = [round(i/10,1),round(j/10,1),round(1-i/10-j/10,1)]
                else:
                    pass

                #pond1 = round(i/10,1)
                #pond2 = round(1-i/10-j/10,1)
                #pond3 = round(j/10,1)
                #clave = str(pond1) + "," + str(pond2) + "," + str(pond3)
                lista_rendimientos = calcular_rend_diarios()
                rent_cartera = rentabilidades[0] * pond[0] + rentabilidades[1] * pond[1] + rentabilidades[2] * pond[2]
                volat_cartera = func_estadistica.calcular_volatilidad_cartera(lista_rendimientos[0], lista_rendimientos[1], lista_rendimientos[2], pond[0], pond[1], pond[2])     
                
                if (volat_cartera >= ran_vol_conservador[0]) and (volat_cartera <= ran_vol_conservador[1]):
                    if max_rent_conservador == None:
                        max_rent_conservador = rent_cartera
                        vol_conservador = volat_cartera
                        pond_conservador = [pond[0], pond[1], pond[2]]
                    elif rent_cartera > max_rent_conservador: 
                        max_rent_conservador = rent_cartera
                        vol_conservador = volat_cartera
                        pond_conservador = [pond[0], pond[1], pond[2]]
                    else:
                        pass
                elif (volat_cartera > ran_vol_moderado[0]) and (volat_cartera <= ran_vol_moderado[1]):
                    if max_rent_moderado == None:
                        max_rent_moderado = rent_cartera
                        vol_moderado = volat_cartera
                        pond_moderado = [pond[0], pond[1], pond[2]]
                    elif rent_cartera > max_rent_moderado: 
                        max_rent_moderado = rent_cartera
                        vol_moderado = volat_cartera
                        pond_moderado = [pond[0], pond[1], pond[2]]
                    else:
                        pass
                elif (volat_cartera > ran_vol_dinamico):
                    if max_rent_dinamico == None:
                        max_rent_dinamico = rent_cartera
                        vol_dinamico = volat_cartera
                        pond_dinamico = [pond[0], pond[1], pond[2]]
                    elif rent_cartera > max_rent_dinamico: 
                        max_rent_dinamico = rent_cartera
                        vol_dinamico = volat_cartera
                        pond_dinamico = [pond[0], pond[1], pond[2]]
                    else:
                        pass
                else:
                    pass

                #d_resultados[clave] = [rent_cartera,volat_cartera]

            resultados = [[[pond_conservador],max_rent_conservador,vol_conservador],
            [[pond_moderado],max_rent_moderado,vol_moderado],
            [[pond_dinamico],max_rent_dinamico,vol_dinamico]]

    if resultado_test <= 2: 
    
        print (f' Tu perfil es conservador y la cartera óptima es {resultados [0]}')
        print(f' La cartera para el perfil moderado es {resultados [1]}')
        print (f' La cartera para el perfil dinamico es {resultados [2]} ')
    
    elif resultado_test > 2 and resultado_test <= 4: 
        
        print (f' Tu perfil es moderado y la cartera óptima es {resultados [1]}')
        print(f' La cartera para el perfil conservador es {resultados [0]}')
        print (f' La cartera para el perfil dinamico es {resultados [2]} ')
    
    elif resultado_test > 4:
        
        print (f' Tu perfil es dinámico y la cartera óptima es {resultados [2]}')
        print(f' La cartera para el perfil conservador es {resultados [0]}')
        print (f' La cartera para el perfil moderado es {resultados [1]} ')
    




    
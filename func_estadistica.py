import statistics
import math

#lista1 = [1,3,5]
#lista2 = [2,6,8]
#lista3 = [25,32,50]
#pond1 = 0.2
#pond2 = 0.2
#pond3 = 0.6

"""Función para calcular la varianza de una lista de rendimientos"""

def calcular_varianza (lista1): 
    media = statistics.mean (lista1)
    num_varianza = 0
    varianza = 0
    for j in lista1: 
        num_varianza += math.pow((j-media),2)
    varianza = num_varianza / len (lista1)
    #print(f'La varianza es: {varianza}')
    return varianza

"""Función para calcular la desviación tipica de una lista de rendimientos"""

def calcular_desv_tipica(lista1):
    desv_tipica = math.sqrt(calcular_varianza(lista1))
    #print(f' La desviación tipica es: {desv_tipica}')
    return desv_tipica

"""Función para calcular la desviación tipica de dos activos""" 

def calcular_covarianza(lista1,lista2):
    media1 = statistics.mean(lista1)
    media2 = statistics.mean(lista2)

    sum = 0
    for t in zip(lista1,lista2):
        sum +=  (t[0]-media1)*(t[1]-media2)
    cov = sum / len(lista1)    
    #print(f' La covarianza es {cov}')
    return cov

""" Cálculo de la volatilidad de una cartera de 3 activos"""

def calcular_volatilidad_cartera (lista1, lista2, lista3, pond1, pond2, pond3):
    vol = math.sqrt(((pond1**2)*(calcular_varianza(lista1))
    +(pond2**2)*(calcular_varianza(lista2))
    +(pond3**2)*(calcular_varianza(lista3))
    +2*(pond1*pond2)*calcular_covarianza(lista1,lista2)
    +2*(pond1*pond3)*calcular_covarianza(lista1,lista3)
    +2*(pond2*pond3)*calcular_covarianza(lista2,lista3))**(1/2))
    return vol

#print(f'La volatilidad de la cartera es: {calcular_volatilidad_cartera(lista1, lista2, lista3, pond1, pond2, pond3)}')

#def suma_volatilidades(lista1, lista2, lista3, pond1, pond2, pond3): 
#    sum_vol = pond1*calcular_desv_tipica(lista1)+pond2*calcular_desv_tipica(lista2)+pond3*calcular_desv_tipica(lista3)
#    return sum_vol

#print(f'La volatilidad de la cartera es: {calcular_volatilidad_cartera(lista1, lista2, lista3, pond1, pond2, pond3)}')
#print(f'La suma de las volatilidades es: {suma_volatilidades(lista1, lista2, lista3, pond1, pond2, pond3)}')

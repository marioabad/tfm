
import leer_fichero_vlp



"""Incluimos las preguntas y respuestas de cada uno de los bloques y seccciones"""
bloques = ["Conocimientos y Experiencia", "Objetivos", "Situación Financiera"]

secciones = [[["Estudios","Selecciona los estudios que estás cursando o has finalizado"], 
["Experiencia","Por favor, cuéntanos si tienes experiencia laboral en mercados financieros"], 
["Conocimientos","¿Estás familiarizado con los principales conceptos y riesgos de los mercados financieros"], 
["Productos","¿Has contratado alguno de estos productos en los últimos tres años?"]], 
[["Objetivo","¿Qué buscas a la hora de invertir?"], 
["Horizonte temporal","¿Durante cuánto tiempo tienes pensado mantener tu inversión?"], 
["Capacidad de asumir pérdidas","En el caso de un año negativo en los mercados financieros, ¿qué caída temporal estarías dispuestoa asumir?"]],
[["Ingresos y Gastos","¿Cuánto crees que puedes ahorrar al mes?"],
["Inversión sobre patrimonio","¿Qué porcentaje supone la inversión sobre tu patrimonio?"], 
["Expectativa de reserva","¿Durante cuánto tiempo podrías cubrir tus gastos actuales sólo con tus ahorros?"]]]

preguntas = [[[["Estudios universitarios relacionados con la economía o los mercados financieros",10],
["Resto de estudios de grado superior",7],
["Formación profesional, Bachillerato y/o Graduado Escolar",3],
["Sin estudios",1]],
[["Si, durante más de un año",10], 
["No, pero soy empresario, autónomo, directivo, ejecutivo o funcionario con conocimiento del sector financiero", 7], 
["Ninguna de las anteriores", 1]],
[["No conozco su funcionamiento ni los riesgos que conlleva", 1], 
["Si, conozco los conceptos generales de los instrumentos financieros y mercados de valores, y entiendo que afectan al valor de las inversiones", 3],
["Entiendo los productos de inversión, los mercados de valores y los riesgos derivados de invertir en ellos", 7], 
["Sé cómo funcionan los productos de inversión, incluidos los de mayor complejidad y los riesgos que implican. Comprendo los términos de volatilidad y correlación", 10]],
[["Depósitos bancarios", 1],
["Renta Fija no compleja", 3],
["Renta variable cotizada",5],
["Fondos, Planes, Seguros de inversión, ETFs", 7],
["Productos estructurados, Capital Riesgo o gestión alternativa", 10]]],
[[["Proteger mi inversión. Riesgo bajo",1], 
["Construir mi inversión de forma progresiva. Riesgo medio", 3],
["Hacer crecer mi inversión de forma notable. Riesgo alto",7],
["Especular a corto plazo con mi inversión. Riesgo muy alto",10]],
[["Menos de 3 años", 3], 
["Entre 3 y 7 años", 7], 
["Más de 7 años", 10]],
[["0%", 1],
["5%", 3],
["15%", 7], 
["25% ó más", 10]]],
[[["Menos del 15%", 1], 
["Entre 15% y 25%", 3], 
["Entre 25% y 50%", 7], 
["Más del 50%", 10]],
[["Menos del 25%", 10], 
["Entre el 25% y el 50%", 7], 
["Un 50%", 3]],
[["No tengo ahorros", 1], 
["3 meses", 3],
["6 meses", 7],
["Más de meses", 10]]]]

matrizRespuestas = [[],[],[]]
numeracion = ["a","b","c","d","e"]
posibles_respuestas_ini = []
puntuaciones_ini = []

numBloque = 0
numSeccion = 0
numPregunta= 0
numPuntuacion = 0
cont = 0
indice = 0

def recoger_opciones(puntuaciones):
    respuesta = input("Introduce la respuesta a la pregunta:")
    if respuesta in posibles_respuestas:
        for i in range (0, len(posibles_respuestas)):
            if respuesta == posibles_respuestas[i]:
                matrizRespuestas[numBloque].insert(numSeccion,puntuaciones[i])
    else:
        print(f'No es una opción válida')
        recoger_opciones(puntuaciones)

for i in bloques:
    posibles_respuestas = []
    puntuaciones = []
    for j in range(0,len(secciones[numBloque])):
        print(f'Bloque {numBloque+1}: {bloques[numBloque]}')
        print(f' {numSeccion+1}: {secciones[numBloque][numSeccion][1]}')
        for k in range(0, len(preguntas[numBloque][numSeccion])):
            print(f' {numeracion[numPregunta]}) {preguntas[numBloque][numSeccion][numPregunta][0]}')
            posibles_respuestas.append(numeracion[numPregunta])
            puntuaciones.append(preguntas[numBloque][numSeccion][numPregunta][1])
            numPregunta +=1


        recoger_opciones(puntuaciones)
        posibles_respuestas.clear
        posibles_respuestas = posibles_respuestas_ini.copy()
        puntuaciones.clear
        puntuaciones = puntuaciones_ini.copy()
        numPregunta = 0
        numSeccion +=1
    numSeccion = 0
    numBloque +=1
print (matrizRespuestas)

""" Cálculo de soluciones"""

matriz_valoraciones = []
valoracion_final = []

for i in matrizRespuestas:
    valoracion = 0 
    for j in range (0, len(i)):
        valoracion += i[j]
    matriz_valoraciones.append(valoracion)
print(matriz_valoraciones)

for i in range (0,3):
    if i == 0:   
        if (matriz_valoraciones[0] <= 10):
            valoracion_final.append(0)
        elif matriz_valoraciones[0] > 10 and matriz_valoraciones[0] <= 20: 
            valoracion_final.append(1)
        elif matriz_valoraciones[0] > 20: 
            valoracion_final.append(2)
    elif i == 1:
        if matriz_valoraciones[1] <= 8:
            valoracion_final.append(0)
        elif matriz_valoraciones[1] > 8 and matriz_valoraciones[1] <= 20: 
            valoracion_final.append(1)
        else: 
            valoracion_final.append(2)
    elif i == 2: 
        if matriz_valoraciones[2] <= 8:
            valoracion_final.append(0)
        elif matriz_valoraciones[2] > 8 and matriz_valoraciones[2] <= 20: 
            valoracion_final.append(1)
        else: 
            valoracion_final.append(2)
print(valoracion_final)

resultado_test = 0

for i in valoracion_final: 
    resultado_test += i
print(resultado_test)

"""Presentamos el resultado del test que nos indica el perfil del cliente"""

if resultado_test <= 2: 
    print(f' El perfil del cliente es Conservador')
    leer_fichero_vlp.calcular_carteras_optimas(resultado_test)

elif resultado_test > 2 and resultado_test <= 4:
    print(f' El perfil del cliente es Moderado')
    leer_fichero_vlp.calcular_carteras_optimas(resultado_test)

else: 
    print(f' El perfil del cliente es Arriesgado')
    leer_fichero_vlp.calcular_carteras_optimas(resultado_test)
  